<?php

$current_plugin = $this->params['plugin'];
$current_page = $this->params['action'];

?>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Ribena E-greeting Backoffice</a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
      <?php if(!empty($userDetails)):?>
         <li><?php echo $this->Html->link('Logout',array('plugin'=>'acl_management','controller'=>'users','action'=>'logout'));?></li>
        <?php endif;?>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>