<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'Ribena');
?>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# ribenabeme: http://ogp.me/ns/fb/ribenabeme#">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php 
	$p = array();
	if(isset($_GET) && count($_GET) > 0) {
		$p = $_GET;
	} else {
		$p = $_POST;
	}

	// debug($p);
	if(empty($p['imgDir'])){
		$p['imgDir'] = '';
	}

	if(empty($p['imgName'])){
		$p['imgName'] = '';
	}

	if(empty($p['name'])){
		$p['name'] = '';
	}

	if(isset($this->params['url'])){
		$this->params['url'] =  $this->params['url'];;
	}else{
		$this->params['url'] = '';
	}

	?>
	<meta property="og:title" content="Ribena Chinese New Year eGreeting"/>
	<meta property="og:type" content="website"/>
	<?php if(empty($p['imgDir'])):?>
		<meta property="og:url" content="https://ribenabeme.com/greeting"/>
	<?php else:?>
		<meta property="og:url" content="https://ribenabeme.com/greeting/v/?imgDir=<?php echo $p['imgDir'];?>&imgName=<?php echo $p['imgName'];?>&name=<?php echo urldecode($p['name']);?>"/>
	<?php endif;?>
	<?php if(empty($p['name'])):?>
		<meta property="og:description" content="Customise Your Chinese New Year Message!"/>
	<?php else:?>
		<meta property="og:description" content="From :<?php echo $p['name'];?>"/>
	<?php endif;?>

	<?php if(empty($p['imgDir'])):?>
		<meta property="og:image" itemprop="image" content="https://ribenabeme.com/greeting/img/video_bg_new.png"/>
	<?php else:?>
		<meta property="og:image" itemprop="image" content="https://ribenabeme.com/greeting/files/<?php echo $p['imgDir'];?>/img_<?php echo $p['imgName'];?>.png"/>
		<meta property="og:image:secure_url" itemprop="image" content="https://ribenabeme.com/greeting/files/<?php echo $p['imgDir'];?>/img_<?php echo $p['imgName'];?>.png"/>
	<?php endif;?>
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:height" content="300" />
	<meta property="og:image:width" content="300" />

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>
		<?php echo $cakeDescription ?>:
		Create Yours Now!
	</title>
	<?php

		//bootstrap css
	echo $this->Html->css('bootstrap.min');
	echo $this->Html->css('bootstrap-theme.min');


		//custom css file where you can use it later
	echo $this->Html->css('custom');

	// echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->Html->script('libs/jquery');
	?>

</head>
<body>
	<div class="container">
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
	</div>
	<?php
	echo $this->Html->script('bootstrap-maxlength/src/bootstrap-maxlength');
	echo $this->Html->script('libs/modernizr.min');
	echo $this->Html->script('libs/bootstrap.min');
	echo $this->fetch('script');
	?>

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-88632376-1', 'auto');
		ga('send', 'pageview');

	</script>
</body>
</html>
