<style type="text/css">
	video::-webkit-media-controls {
		overflow: hidden !important
	}
	video::-webkit-media-controls-enclosure {
		width: calc(100% + 32px);
		margin-left: auto;
	}

</style>
<div class="row">
	<div class="col-sm-6 col-sm-offset-3 col-md-4 col-lg-12 col-lg-offset-0">
		<div class="panel panel-default fc-panel">
			
			<a href="<?php echo $this->webroot;?>msg" data-toggle="tooltip" data-placement="top" title="Click here">
				<?php echo $this->Html->image('Ribena-Logo.png',array('class'=>'ribena-logo img-responsive'));?>
			</a>
			
		</div>
	</div>
</div>
<div class="row"> 
	<div class="col-xs-12 col-sm-12 col-lg-8 col-lg-offset-2 vcenter">
		<video id="video" controls>
			<source src="https://ribenabeme.com/greeting/base_vid/ribena3.mp4" type="video/mp4">
				Your browser does not support HTML5 video.
			</video>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-6 col-sm-offset-3 col-md-4 col-lg-12 col-lg-offset-0">
		<div class="panel panel-default fc-panel">
			<div class="panel-heading wrapper">
				<a href="<?php echo $this->webroot;?>msg" data-toggle="tooltip" data-placement="top" title="Click here">
					<?php echo $this->Html->image('customise-01.png',array('class'=>'greet img-responsive'));?>
				</a>
			</div>
		</div>
	</div>
</div>