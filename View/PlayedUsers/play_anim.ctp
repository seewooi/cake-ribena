<?php 
$domain = 'https://ribenabeme.com';
$url = 'https://ribenabeme.com/greeting/';

$p = array();
if(isset($_GET) && count($_GET) > 0) {
  $p = $_GET;
} else {
  $p = $_POST;
}

if(empty($p['imgDir'])){
  $p['imgDir'] = '';
  header('Location:' . $url);
  exit();
}

if(empty($p['imgName'])){
  $p['imgName'] = '';
  header('Location:' . $url);
  exit();
}

if(empty($p['name'])){
  $p['name'] = '';
  header('Location:' . $url);
  exit();
}

?>
<style type="text/css">
  body {
    margin: 0;
  }
  video {
    width: 100%;
    height: auto;
  }
  .vcenter {
    display: inline-block;
    vertical-align: middle;
    float: none;
    margin-top: 10%;
  }

  #greetingMessage{
    color:white;
  }
  video::-webkit-media-controls {
    overflow: hidden !important
  }
  video::-webkit-media-controls-enclosure {
    width: calc(100% + 32px);
    margin-left: auto;
  }

</style>
<div class="row"> 
  <div class="col-xs-12 col-sm-12 col-lg-8 col-lg-offset-2 vcenter">
    <video id="video" autoplay controls>
     <source src="<?php echo $url;?><?php echo $getPlayData['PlayedUser']['vidLink'];?>" type="video/mp4">
      Your browser does not support HTML5 video.
    </video>
    <div class="text-center">
      <h3>Share:</h3>
      <div class="a2a_kit a2a_kit_size_32 a2a_default_style text-center" id="sharebtn">
        <a class="a2a_button_whatsapp"></a>
        <a class="a2a_button_facebook"></a>
        <a class="a2a_button_wechat"></a>
        <a class="a2a_button_line"></a>
        <a class="a2a_button_twitter"></a>
        <a class="a2a_button_google_plus"></a>
        <a class="a2a_dd" href="https://www.addtoany.com/share?linkurl=http%3A%2F%2Fribenabeme.com%2Fcny%2Fv%2F%3FimgDir%3D2016-12-07%26imgName%3D20161207123625%26name%3Dfariz%2520%2526%2520family&amp;linkname="></a>
      </div>
      <script>
        var a2a_config = a2a_config || {};
        var ua = navigator.userAgent.toLowerCase();
        a2a_config.linkurl = "<?php echo $domain;?><?php echo $this->request->here;?>?imgDir=<?php echo $p['imgDir'];?>&imgName=<?php echo $p['imgName'];?>&name=<?php echo urlencode($p['name']);?>";

      </script>
      <script async src="https://static.addtoany.com/menu/page.js"></script>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="text-center">
      <a href="<?php echo $this->webroot;?>msg" data-toggle="tooltip" data-placement="top" title="Click here">
        <?php echo $this->Html->image('customise-01.png',array('class'=>'greet img-responsive'));?>
      </a>
    </div>
  </div>
</div>
