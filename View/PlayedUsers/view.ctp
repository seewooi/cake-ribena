<div class="playedUsers view">
<h2><?php echo __('Played User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($playedUser['PlayedUser']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($playedUser['PlayedUser']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Message'); ?></dt>
		<dd>
			<?php echo h($playedUser['PlayedUser']['message']); ?>
			&nbsp;
		</dd>
		<td><img class="img-responsive" src="<?php echo $this->webroot;?>files/<?php echo h($playedUser['PlayedUser']['fileLink']); ?>">&nbsp;</td>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($playedUser['PlayedUser']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($playedUser['PlayedUser']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Played User'), array('action' => 'edit', $playedUser['PlayedUser']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Played User'), array('action' => 'delete', $playedUser['PlayedUser']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $playedUser['PlayedUser']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Played Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Played User'), array('action' => 'add')); ?> </li>
	</ul>
</div>
