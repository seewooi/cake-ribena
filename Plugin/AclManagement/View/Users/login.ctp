
<fieldset>



</fieldset>

<div class="container" style="margin-top:40px">
    <div class="row">
    <div class="col-sm-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong> Login to continue</strong>
                </div>
                <div class="panel-body">
                    <form role="form" action="#" method="POST">
                        <fieldset>
                            <div class="row">
                                <div class="center-block">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-10 ">
                                    <?php
                                    echo $this->Form->create('User', array('url' => 'login', 'class'=>'form-horizontal'));
                                    ?>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-user"></i>
                                            </span> 
                                            <?php 
                                            echo $this->Form->input('email', array('div'=>false,
                                                'after'=>$this->Form->error('email', array(), array('wrap' => 'span', 'class' => 'help-inline')),
                                                'error' => array('attributes' => array('style' => 'display:none')),
                                                'label'=>false, 'class'=>'form-control','autofocus','placeholder'=>'Email'));
                                                ?>
                                                <!-- <input class="form-control" placeholder="Username" name="loginname" type="text" autofocus> -->
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-lock"></i>
                                                </span>
                                                <?php
                                                echo $this->Form->input('password', array('div'=>false,
                                                    'after'=>$this->Form->error('password', array(), array('wrap' => 'span', 'class' => 'help-inline')).'</div>',
                                                    'error' => array('attributes' => array('style' => 'display:none')),
                                                    'label'=>false, 'class'=>'form-control','autofocus','placeholder'=>'Password'));
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="text-center">
                                                    <?php echo $this->Form->submit(__('Login'), array('class'=>'btn btn-primary', 'div'=>false));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <?php
                                echo $this->Form->end();
                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>