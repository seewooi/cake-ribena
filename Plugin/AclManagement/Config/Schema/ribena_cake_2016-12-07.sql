# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 192.168.33.10 (MySQL 5.5.49-0ubuntu0.14.04.1)
# Database: ribena_cake
# Generation Time: 2016-12-06 16:08:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table acos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acos`;

CREATE TABLE `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`)
VALUES
	(1,NULL,NULL,NULL,'controllers',1,82),
	(2,1,NULL,NULL,'Pages',2,5),
	(3,2,NULL,NULL,'display',3,4),
	(4,1,NULL,NULL,'AclManagement',6,59),
	(5,4,NULL,NULL,'Groups',7,18),
	(6,5,NULL,NULL,'index',8,9),
	(7,5,NULL,NULL,'view',10,11),
	(8,5,NULL,NULL,'add',12,13),
	(9,5,NULL,NULL,'edit',14,15),
	(10,5,NULL,NULL,'delete',16,17),
	(11,4,NULL,NULL,'UserPermissions',19,28),
	(12,11,NULL,NULL,'index',20,21),
	(13,11,NULL,NULL,'sync',22,23),
	(14,11,NULL,NULL,'edit',24,25),
	(15,11,NULL,NULL,'toggle',26,27),
	(16,4,NULL,NULL,'Users',29,58),
	(17,16,NULL,NULL,'login',30,31),
	(18,16,NULL,NULL,'logout',32,33),
	(19,16,NULL,NULL,'index',34,35),
	(20,16,NULL,NULL,'view',36,37),
	(21,16,NULL,NULL,'add',38,39),
	(22,16,NULL,NULL,'edit',40,41),
	(23,16,NULL,NULL,'delete',42,43),
	(24,16,NULL,NULL,'toggle',44,45),
	(25,16,NULL,NULL,'register',46,47),
	(26,16,NULL,NULL,'confirm_register',48,49),
	(27,16,NULL,NULL,'forgot_password',50,51),
	(28,16,NULL,NULL,'activate_password',52,53),
	(29,16,NULL,NULL,'edit_profile',54,55),
	(30,16,NULL,NULL,'confirm_email_update',56,57),
	(31,1,NULL,NULL,'PlayedUsers',60,81),
	(32,31,NULL,NULL,'index',61,62),
	(33,31,NULL,NULL,'view',63,64),
	(34,31,NULL,NULL,'add',65,66),
	(35,31,NULL,NULL,'edit',67,68),
	(36,31,NULL,NULL,'delete',69,70),
	(37,31,NULL,NULL,'playAnim',71,72),
	(38,31,NULL,NULL,'getPlayData',73,74),
	(39,31,NULL,NULL,'preview',75,76),
	(40,31,NULL,NULL,'share',77,78),
	(41,31,NULL,NULL,'test',79,80);

/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table aros
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aros`;

CREATE TABLE `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`)
VALUES
	(1,NULL,'Group',1,NULL,1,4),
	(2,1,'User',1,NULL,2,3);

/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table aros_acos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aros_acos`;

CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`)
VALUES
	(1,1,1,'1','1','1','1');

/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `created`, `modified`)
VALUES
	(1,'admin','2016-11-30 06:14:50','2016-11-30 06:14:50');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table played_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `played_users`;

CREATE TABLE `played_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `message` text,
  `fileLink` text,
  `vidLink` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `played_users` WRITE;
/*!40000 ALTER TABLE `played_users` DISABLE KEYS */;

INSERT INTO `played_users` (`id`, `name`, `message`, `fileLink`, `vidLink`, `created`, `modified`)
VALUES
	(1,'FARIZ','msg1','2016-12-01/img_20161201100246.png',NULL,'2016-12-01 10:02:48','2016-12-01 10:02:48'),
	(2,'Fariz','msg3','2016-12-01/img_20161201101940.png',NULL,'2016-12-01 10:19:42','2016-12-01 10:19:42'),
	(3,'FARIZ','msg1','2016-12-01/img_20161201105430.png',NULL,'2016-12-01 10:54:33','2016-12-01 10:54:33'),
	(4,'FARIZ','msg2','2016-12-01/img_20161201105609.png',NULL,'2016-12-01 10:56:11','2016-12-01 10:56:11'),
	(5,'FARIZ','msg2','2016-12-01/img_20161201105654.png',NULL,'2016-12-01 10:56:56','2016-12-01 10:56:56'),
	(6,'FARIZ','msg2','2016-12-01/img_20161201105730.png',NULL,'2016-12-01 10:57:33','2016-12-01 10:57:33'),
	(7,'FARIZ','msg2','2016-12-01/img_20161201105804.png',NULL,'2016-12-01 10:58:07','2016-12-01 10:58:07'),
	(8,'FARIZ','msg2','2016-12-01/img_20161201105845.png',NULL,'2016-12-01 10:58:47','2016-12-01 10:58:47'),
	(9,'FARIZ','msg1','2016-12-01/img_20161201105938.png',NULL,'2016-12-01 10:59:40','2016-12-01 10:59:40'),
	(10,'FARIZ','msg1','2016-12-01/img_20161201105957.png',NULL,'2016-12-01 10:59:59','2016-12-01 10:59:59'),
	(11,'FARIZ','msg1','2016-12-01/img_20161201110018.png',NULL,'2016-12-01 11:00:20','2016-12-01 11:00:20'),
	(12,'FARIZ','msg1','2016-12-01/img_20161201110025.png',NULL,'2016-12-01 11:00:27','2016-12-01 11:00:27'),
	(13,'FARIZ','msg1','2016-12-01/img_20161201110103.png',NULL,'2016-12-01 11:01:05','2016-12-01 11:01:05'),
	(14,'FARIZ','msg1','2016-12-01/img_20161201110159.png',NULL,'2016-12-01 11:02:02','2016-12-01 11:02:02'),
	(15,'FARIZ','msg1','2016-12-01/img_20161201110230.png',NULL,'2016-12-01 11:02:32','2016-12-01 11:02:32'),
	(16,'FARIZ','msg1','2016-12-01/img_20161201110258.png',NULL,'2016-12-01 11:03:00','2016-12-01 11:03:00'),
	(17,'FARIZ','msg1','2016-12-01/img_20161201110312.png',NULL,'2016-12-01 11:03:14','2016-12-01 11:03:14'),
	(18,'FARIZ','msg1','2016-12-01/img_20161201110343.png',NULL,'2016-12-01 11:03:45','2016-12-01 11:03:45'),
	(19,'FARIZ','msg1','2016-12-01/img_20161201110433.png',NULL,'2016-12-01 11:04:35','2016-12-01 11:04:35'),
	(20,'FARIZ','msg1','2016-12-01/img_20161201110455.png',NULL,'2016-12-01 11:04:58','2016-12-01 11:04:58'),
	(21,'FARIZ IZWAN','msg1','2016-12-01/img_20161201110656.png',NULL,'2016-12-01 11:06:59','2016-12-01 11:06:59'),
	(22,'far','msg1','2016-12-01/img_20161201120544.png',NULL,'2016-12-01 12:05:47','2016-12-01 12:05:47'),
	(23,'Fariz Izwan & Family','msg1','2016-12-01/img_20161201121311.png',NULL,'2016-12-01 12:13:13','2016-12-01 12:13:13'),
	(24,'FARIZ','msg1','2016-12-02/img_20161202105905.png',NULL,'2016-12-02 10:59:08','2016-12-02 10:59:08'),
	(25,'FARIZ','msg1','2016-12-02/img_20161202110050.png',NULL,'2016-12-02 11:00:51','2016-12-02 11:00:51'),
	(26,'FARIZ','msg1','2016-12-02/img_20161202110141.png',NULL,'2016-12-02 11:01:42','2016-12-02 11:01:42'),
	(27,'fariz','msg1','2016-12-03/img_20161203022650.png',NULL,'2016-12-03 02:26:52','2016-12-03 02:26:52'),
	(28,'fariz','msg1','2016-12-03/img_20161203022706.png',NULL,'2016-12-03 02:27:07','2016-12-03 02:27:07'),
	(29,'fariz','msg1','2016-12-03/img_20161203022728.png',NULL,'2016-12-03 02:27:29','2016-12-03 02:27:29'),
	(30,'fariz','msg1','2016-12-03/img_20161203023101.png',NULL,'2016-12-03 02:31:03','2016-12-03 02:31:03'),
	(31,'fariz','msg1','2016-12-03/img_20161203031812.png',NULL,'2016-12-03 03:18:14','2016-12-03 03:18:14'),
	(32,'test','msg1','2016-12-04/img_20161204054324.png',NULL,'2016-12-04 05:43:25','2016-12-04 05:43:25'),
	(33,'test','msg1','2016-12-04/img_20161204055001.png',NULL,'2016-12-04 05:50:02','2016-12-04 05:50:02'),
	(34,'test','msg1','2016-12-04/img_20161204055951.png',NULL,'2016-12-04 05:59:52','2016-12-04 05:59:52'),
	(35,'test','msg1','2016-12-04/img_20161204060014.png',NULL,'2016-12-04 06:00:15','2016-12-04 06:00:15'),
	(36,'FARIZ','msg1','2016-12-04/img_20161204063247.png',NULL,'2016-12-04 06:32:48','2016-12-04 06:32:48'),
	(37,'FARIZ IZWAN','msg1','2016-12-04/img_20161204063430.png',NULL,'2016-12-04 06:34:31','2016-12-04 06:34:31'),
	(38,'FARIZ IZWAN','msg1','2016-12-04/img_20161204065117.png',NULL,'2016-12-04 06:51:18','2016-12-04 06:51:18'),
	(39,'FARIZ IZWAN','msg1','2016-12-04/img_20161204065909.png',NULL,'2016-12-04 06:59:10','2016-12-04 06:59:10'),
	(40,'FARIZ IZWAN','msg1','2016-12-04/img_20161204070915.png',NULL,'2016-12-04 07:09:16','2016-12-04 07:09:16'),
	(41,'FARIZ IZWAN KAMARUZZAMAN','msg2','2016-12-04/img_20161204071536.png',NULL,'2016-12-04 07:15:37','2016-12-04 07:15:37'),
	(42,'FARIZ IZWAN KAMARUZZAMAN','msg2','2016-12-04/img_20161204071743.png',NULL,'2016-12-04 07:17:44','2016-12-04 07:17:44'),
	(43,'fariz & family','msg1','2016-12-04/img_20161204071837.png',NULL,'2016-12-04 07:18:38','2016-12-04 07:18:38'),
	(44,'fariz & family','msg1','2016-12-04/img_20161204072425.png',NULL,'2016-12-04 07:24:26','2016-12-04 07:24:26'),
	(45,'fariz & family','msg1','2016-12-04/img_20161204072900.png',NULL,'2016-12-04 07:29:01','2016-12-04 07:29:01'),
	(46,'fariz & family','msg1','2016-12-04/img_20161204073156.png',NULL,'2016-12-04 07:31:56','2016-12-04 07:31:56'),
	(47,'fariz & family','msg1','2016-12-04/img_20161204073207.png',NULL,'2016-12-04 07:32:08','2016-12-04 07:32:08'),
	(48,'fariz','msg1','2016-12-04/img_20161204075126.png',NULL,'2016-12-04 07:51:27','2016-12-04 07:51:27'),
	(49,'FARIZ','msg2','2016-12-04/img_20161204082319.png',NULL,'2016-12-04 08:23:20','2016-12-04 08:23:20'),
	(50,'hahaha','msg1','2016-12-04/img_20161204083117.png',NULL,'2016-12-04 08:31:18','2016-12-04 08:31:18'),
	(51,'FARIZ IZWAN & FAMILY','msg1','2016-12-04/img_20161204173850.png',NULL,'2016-12-04 17:38:51','2016-12-04 17:38:51'),
	(52,'FARIZ IZWAN & FAMILY','msg1','2016-12-04/img_20161204175333.png',NULL,'2016-12-04 17:53:34','2016-12-04 17:53:34'),
	(53,'FARIZ IZWAN HOPENA','msg1','2016-12-04/img_20161204175513.png',NULL,'2016-12-04 17:55:13','2016-12-04 17:55:13');

/*!40000 ALTER TABLE `played_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `body` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` tinytext COLLATE utf8_unicode_ci COMMENT 'full url to avatar image file',
  `language` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `group_id`, `username`, `name`, `password`, `email`, `avatar`, `language`, `timezone`, `token`, `status`, `created`, `modified`, `last_login`)
VALUES
	(1,1,NULL,'admin','0ccc35f8be81c64a46b6b50dc4e88b24c09c03ee','admin@aqm.com.my',NULL,NULL,NULL,NULL,1,'2016-11-30 06:15:13','2016-11-30 06:15:13','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
