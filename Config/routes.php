<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 */

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'playedUsers', 'action' =>'landing'));
Router::connect('/msg', array('controller' => 'playedUsers', 'action' =>'add'));
Router::connect('/getPlayerInfo', array('controller' => 'playedUsers', 'action' =>'getPlayData'));
Router::connect('/pv/*', array('controller' => 'playedUsers', 'action' =>'preview'));
Router::connect('/v/*', 
	array('controller' => 'playedUsers', 'action' =>'playAnim'));
Router::connect('/testff', array('controller' => 'playedUsers', 'action' =>'test'));
Router::connect('/share', array('controller' => 'playedUsers', 'action' =>'share'));
Router::connect('/play/records', array('controller' => 'playedUsers', 'action' =>'index'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

	/**
 * REST Api routes
 */
	Router::mapResources(array('rest_apis'));
	Router::parseExtensions('json','php');

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
